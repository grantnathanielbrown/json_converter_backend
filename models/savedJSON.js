const mongoose = require("../db/connection");

const savedJSON = new mongoose.Schema({
    uniqueID: String,
    body: Object,
    createdAt: Date
});
// consider adding comments 

const JSON = mongoose.model("JSON", savedJSON);

module.exports = JSON;